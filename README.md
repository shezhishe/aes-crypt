# AES加密解密

## 环境要求

1. php>=5.3
2. openssl扩展

## 测试

1. composer install
2. test/Test.php 简单测试demo
3. php test/Test.php

## 开始使用

``
composer require she-zhi-she/aes-crypt
``

+ 简单使用

    ```php
    use SheZhiShe\AesCrypt;
    require_once __DIR__.'/../vendor/autoload.php';
    $ase=new AesCrypt();
    //加密
    $res=$ase->encode('d12347477');
    var_dump($res->getRes());
    //解密
    $dres=$ase->key($res->getKey())->iv($res->getIv())->decode($res->getRes());
    var_dump($dres->getRes());
    ```

+ 前端使用
    > 注意前后端要注意key的长度 aes-128-cbc 16位 256 32位 ，key,iv前后端要固定  
    > 后端的$potions需要设置为1，默认是0

    ```js
    import CryptoJS from 'crypto-js'
    const Verify = {
        decode: function(encodeStr, sign='') {
            const key = this.getKey(sign)
            const iv = this.getIv(sign)
            const res = CryptoJS.AES.decrypt(encodeStr, key, {
                iv: iv,
                mode: CryptoJS.mode.CBC,
                padding: CryptoJS.pad.Pkcs7
            })
            return JSON.parse(res.toString(CryptoJS.enc.Utf8))
        },
        getIv: function (sign) {
            const iv = sign.slice(0,16)
            return CryptoJS.enc.Utf8.parse(iv)
        },
        getKey: function (sign) {
            const key = sign.slice(4,20)
            return CryptoJS.enc.Utf8.parse(key)
        }
    }

    export default Verify
    ```

## LICENSE

[MIT]('./LICENSE')
